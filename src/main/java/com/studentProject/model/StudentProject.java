package com.studentProject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "student_project")
public class StudentProject {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long proj_id;
	
	@Column(nullable = false)
	private String proj_name;
	
	@Column(nullable = false)
	private String proj_duration;
	
	@Column(nullable = false)
	private Long num_of_students;
	

	public Long getProj_id() {
		return proj_id;
	}
	public void setProj_id(Long proj_id) {
		this.proj_id = proj_id;
	}
	public String getProj_name() {
		return proj_name;
	}
	public void setProj_name(String proj_name) {
		this.proj_name = proj_name;
	}
	public String getProj_duration() {
		return proj_duration;
	}
	public void setProj_duration(String proj_duration) {
		this.proj_duration = proj_duration;
	}
	
	public Long getNum_of_students() {
		return num_of_students;
	}
	public void setNum_of_students(Long num_of_students) {
		this.num_of_students = num_of_students;
	}
	
}
