package com.studentProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.studentProject.model.StudentProject;
import com.studentProject.model.User;
import com.studentProject.repository.UserRepository;
import com.studentProject.service.userService;

import javassist.expr.NewArray;


@Controller
public class AppController {
	
	@Autowired
	private UserRepository repo;
	
	@Autowired
	private userService service;

	@GetMapping("")
	public String viewHomepage() {
		return "index";
	}
	
	@GetMapping("/register")
	public String signUpForm(Model model) {
		
		model.addAttribute("user", new User());
		return "signUp_Form";
	}
	
	@GetMapping("/createProject")
	public String createProject(Model model) {
		
		model.addAttribute("studentproject", new StudentProject());
		return "createProject";
	}
	
	@PostMapping("/process_register")
	public String processValues(User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		
		repo.save(user);
		return "register_successForm";
	}
	
	@GetMapping("/list_users")
	public String viewUsers(Model model) {
		
		List<User> listUsers = repo.findAll();
		model.addAttribute("listUsers", listUsers);
		return "users";
	}
	
	@RequestMapping("/listProjects")
	public String viewProjects(Model model) {
		List<StudentProject> listProjects = service.listAll();
		model.addAttribute("listProjects", listProjects);
		
		return "projects";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("user") User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		
		service.save(user);
		return "Update_successForm";
	}
	

	@RequestMapping(value = "/saveProject", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("studentproject") StudentProject project) {
		service.save(project);
		
		return "redirect:/";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("editUser");
		User user = service.get(id);
		mav.addObject("user", user);

		return mav;
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteUser(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/";		
	}
	
}
