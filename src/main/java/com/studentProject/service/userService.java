package com.studentProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studentProject.model.StudentProject;
import com.studentProject.model.User;
import com.studentProject.repository.ProjectRepository;
import com.studentProject.repository.UserRepository;


@Service
@Transactional
public class userService {

	@Autowired
	private UserRepository repo;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	public void save(User user) {
		repo.save(user);
	}
	
	public User get(long id) {
		return repo.findById(id).get();
	}
	
	public void delete(long id) {
		repo.deleteById(id);
	}
	
	public List<StudentProject> listAll() {		
		return projectRepository.findAll();
	}
	
	public void save(StudentProject project) {
		projectRepository.save(project);
	}
	
	public StudentProject get(Long proj_id) {
		return projectRepository.findById(proj_id).get();
	}
	
	public void delete(Long proj_id) {
		projectRepository.deleteById(proj_id);
	}
}
