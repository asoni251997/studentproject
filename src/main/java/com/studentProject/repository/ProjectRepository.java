package com.studentProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.studentProject.model.StudentProject;

public interface ProjectRepository extends JpaRepository<StudentProject, Long> {

}
